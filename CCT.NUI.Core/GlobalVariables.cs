﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CCT.NUI.Core
{
    public struct Box
    {
        public Rectangle leftBox;
        public Rectangle rightBox;
        public Point midBulkhead;
    }

    public class GlobalVariables
    {
        public static Box TheBox = new Box();
        public static int Counter = 100;
        public static int CameraHeight = 903;
        public static int PartitionHeight = 750;
        public static int FilteredXmin = 100;
        public static int FilteredXmax = 400;
        public static int BlockHeight = 26;
        public static bool ResetDepthFilter = false;
        public static bool ResetDepthBitmap = false;
        public static bool isCalibrating = false;
    }
}

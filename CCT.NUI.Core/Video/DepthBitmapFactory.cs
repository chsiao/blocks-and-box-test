﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Drawing.Imaging;
using System.Runtime.ExceptionServices;
using System.Runtime.InteropServices;
using Emgu;
using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.Util;
using Emgu.CV.Structure;
using Emgu.CV.VideoSurveillance;

namespace CCT.NUI.Core.Video
{
    public class DepthBitmapFactory : DepthImageFactoryBase, IBitmapFactory
    {
        public DepthBitmapFactory(int maxDepth)
            : base(maxDepth)
        { }

        private int[,] presetModel = new int[640, 480];
        private int[,] dMaxShell = new int[640, 480];
        private int[,] dminShell = new int[640, 480];
        private Int16[] presetModelBox = new Int16[640 * 480];
        private int count = 0;        

        [HandleProcessCorruptedStateExceptions]
        public unsafe void CreateImage(Bitmap targetImage, IntPtr pointer, out Bitmap image2ndLvl, out Bitmap image3rdLvl)
        {
            if (GlobalVariables.ResetDepthBitmap)
            {
                presetModel = new int[640, 480];
                dMaxShell = new int[640, 480];
                dminShell = new int[640, 480];
                presetModelBox = new Int16[640 * 480];
                count = 0;

                GlobalVariables.ResetDepthBitmap = false;
            }

            var area = new System.Drawing.Rectangle(0, 0, targetImage.Width, targetImage.Height); 
            this.CreateHistogram(pointer, area.Width, area.Height);

            image2ndLvl = new Bitmap(area.Width, area.Height);
            image3rdLvl = new Bitmap(area.Width, area.Height);

            var data = targetImage.LockBits(area, ImageLockMode.WriteOnly, System.Drawing.Imaging.PixelFormat.Format24bppRgb);
            var data2 = image2ndLvl.LockBits(area, ImageLockMode.WriteOnly, System.Drawing.Imaging.PixelFormat.Format24bppRgb);
            var data3 = image3rdLvl.LockBits(area, ImageLockMode.WriteOnly, System.Drawing.Imaging.PixelFormat.Format24bppRgb);

            try
            {
                ushort* pDepth = (ushort*)pointer;
                for (int y = 0; y < area.Height; y++)
                {
                    byte* pDest = (byte*)data.Scan0.ToPointer() + y * data.Stride;
                    byte* pDest1 = (byte*)data2.Scan0.ToPointer() + y * data2.Stride;
                    byte* pDest2 = (byte*)data3.Scan0.ToPointer() + y * data3.Stride;

                    for (int x = 0; x < area.Width; x++, ++pDepth, pDest += 3, pDest1 += 3, pDest2 += 3)
                    {
                        if (count == 0)
                            presetModel[x, y] = *pDepth;
                        else if (count < GlobalVariables.Counter || GlobalVariables.isCalibrating)
                        {
                            presetModel[x, y] = (*pDepth + presetModel[x, y] * count) / (count + 1);
                        }

                        if (count > GlobalVariables.Counter && !GlobalVariables.isCalibrating)
                        {
                            if (x < GlobalVariables.FilteredXmax && x > GlobalVariables.FilteredXmin)
                            {
                                if (*pDepth < dMaxShell[x, y])
                                {
                                    byte pixel = (byte)histogram.GetValue(*pDepth);
                                    pDest[0] = pixel;
                                    pDest[1] = pixel;
                                    pDest[2] = pixel;
                                }
                                else
                                {
                                    pDest[0] = 0;
                                    pDest[1] = 0;
                                    pDest[2] = 0;
                                }

                                if (*pDepth < dMaxShell[x, y] - GlobalVariables.BlockHeight)
                                {
                                    byte pixel = (byte)histogram.GetValue(*pDepth);
                                    pDest1[0] = pixel;
                                    pDest1[1] = pixel;
                                    pDest1[2] = pixel;
                                }
                                else
                                {
                                    pDest1[0] = 0;
                                    pDest1[1] = 0;
                                    pDest1[2] = 0;
                                }

                                if (*pDepth < dMaxShell[x, y] - (GlobalVariables.BlockHeight * 2))
                                {
                                    byte pixel = (byte)histogram.GetValue(*pDepth);
                                    pDest2[0] = pixel;
                                    pDest2[1] = pixel;
                                    pDest2[2] = pixel;
                                }
                                else
                                {
                                    pDest2[0] = 0;
                                    pDest2[1] = 0;
                                    pDest2[2] = 0;
                                }
                            }
                            else
                            {
                                pDest[0] = 0;
                                pDest[1] = 0;
                                pDest[2] = 0;
                            }
                        }
                        else
                        {
                            byte pixel = (byte)histogram.GetValue(*pDepth);
                            pDest[0] = pixel;
                            pDest[1] = pixel;
                            pDest[2] = pixel;

                            if (count == GlobalVariables.Counter)
                            {
                                if (presetModel[x, y] < 880)
                                {
                                    presetModelBox[x + y * 640] = (Int16)presetModel[x, y];
                                }
                                else
                                    presetModelBox[x + y * 640] = 0;
                            }

                            if (count == GlobalVariables.Counter)
                            {
                                dMaxShell[x, y] = presetModel[x, y] - 7;
                                dminShell[x, y] = presetModel[x, y] - 14;
                            }
                        }

                        //byte pixel = (byte)histogram.GetValue(*pDepth);
                        //pDest[0] = pixel;
                        //pDest[1] = pixel;
                        //pDest[2] = pixel;
                    }
                }

                #region Create Box
                if (false)
                {

                    //CSVUtil.WriteCSV("d:\\test.csv", output);
                    Image<Gray, Int16> EMGUImage = new Image<Gray, Int16>(640, 480);
                    GCHandle handle = GCHandle.Alloc(presetModelBox, GCHandleType.Pinned);
                    int size = Marshal.SizeOf(typeof(MIplImage));
                    IntPtr imageHeaderForBytes = Marshal.AllocHGlobal(size);
                    CvInvoke.cvInitImageHeader(
                          imageHeaderForBytes,
                          new System.Drawing.Size(640, 480),
                          Emgu.CV.CvEnum.IPL_DEPTH.IPL_DEPTH_16S, 1, 0, 4);
                    int offset = (int)Marshal.OffsetOf(typeof(MIplImage), "imageData");
                    Marshal.WriteIntPtr(
                          imageHeaderForBytes,
                          offset,
                          handle.AddrOfPinnedObject());

                    CvInvoke.cvCopy(imageHeaderForBytes, EMGUImage.Ptr, IntPtr.Zero);

                    Marshal.FreeHGlobal(imageHeaderForBytes);
                    handle.Free();

                    EMGUImage = EMGUImage.Erode(1);
                    EMGUImage = EMGUImage.Dilate(1);

                    Image<Gray, byte> canny = EMGUImage.Convert<Gray, byte>().SmoothMedian(7).Canny(new Gray(800), new Gray(200));

                    #region Find the border of box

                    Contour<System.Drawing.Point> blocks;
                    blocks =
                    canny.FindContours(Emgu.CV.CvEnum.CHAIN_APPROX_METHOD.CV_CHAIN_APPROX_SIMPLE, Emgu.CV.CvEnum.RETR_TYPE.CV_RETR_LIST);
                    //-----------------------
                    MCvFont font = new MCvFont(FONT.CV_FONT_HERSHEY_SIMPLEX, 0.5, 0.5);
                    for (; blocks != null; blocks = blocks.HNext)
                    {
                        Contour<System.Drawing.Point> poly = blocks
                            .ApproxPoly(blocks.Perimeter * 0.05,
                                        blocks.Storage);
                        if (poly.Area > 50000 && poly.Total > 3)// && poly.Perimeter <200)// && poly.Convex == true)
                        {
                            EMGUImage.Draw(poly, new Gray(255), 5);
                            System.Drawing.Point[] fillArea = poly.ToArray();
                            int boardThickness = (int)(poly.BoundingRectangle.Width * 0.08F);
                            EMGUImage.FillConvexPoly(fillArea, new Gray(820));

                            fillArea[0].Y = poly.BoundingRectangle.Top + poly.BoundingRectangle.Height / 2 - boardThickness;
                            fillArea[1].Y = poly.BoundingRectangle.Top + poly.BoundingRectangle.Height / 2 + boardThickness;
                            fillArea[2].Y = poly.BoundingRectangle.Top + poly.BoundingRectangle.Height / 2 + boardThickness;
                            fillArea[3].Y = poly.BoundingRectangle.Top + poly.BoundingRectangle.Height / 2 - boardThickness;

                            fillArea[0].X = poly.BoundingRectangle.Right+20;
                            fillArea[1].X = poly.BoundingRectangle.Right+20;
                            fillArea[2].X = poly.BoundingRectangle.Left-20;
                            fillArea[3].X = poly.BoundingRectangle.Left-20;

                            EMGUImage.FillConvexPoly(fillArea, new Gray(720));

                            EMGUImage._Dilate(3);
                        }

                    }


                    #endregion


                    for (int y = 0; y < 480; y++)
                    {
                        for (int x = 0; x < 640; x++)
                        {
                            if (EMGUImage.Data[y, x, 0] > 500)
                            {
                                presetModel[x, y] = EMGUImage.Data[y, x, 0];
                            }

                        }
                    }
                    //CvInvoke.cvShowImage("mask Snap Shot", EMGUImage.Convert<Gray, byte>());
                    //CvInvoke.cvShowImage("canny Snap Shot", canny);
                    //CvInvoke.cvWaitKey(100);
                }
                #endregion
                if (!GlobalVariables.isCalibrating)
                {
                    count++;
                }
            }
            catch (AccessViolationException)
            { }
            catch (SEHException)
            { }

            targetImage.UnlockBits(data);
            image2ndLvl.UnlockBits(data2);
            image3rdLvl.UnlockBits(data3);

#if false
            if (image2ndLvl != null)
            {
                Image<Gray, byte> tmpImage2Lvl = new Image<Gray, byte>(image2ndLvl);
                CvInvoke.cvShowImage("2nd level", tmpImage2Lvl);
            }
            image2ndLvl = null;
            image3rdLvl = null;
#endif
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Collections.Concurrent;
using System.Runtime.ExceptionServices;
using System.Runtime.InteropServices;
using OpenNI;

namespace CCT.NUI.Core.OpenNI
{
    public class OpenNIRunner 
    {
        private bool existing = false;
        private ConcurrentBag<IGenerator> generators;

        private Context context;
        private Thread thread;
        private bool run;
        //private Recorder record;
        private Player player;

        public OpenNIRunner(Context context, Player player = null)
        {
            
            lock (typeof(OpenNIRunner))
            {
                if (existing)
                {
                    throw new NotSupportedException("Only one instance of OpenNIRunner must exist");
                }
                existing = true;
            }
            this.context = context;
            this.player = player;

            //this.record = new Recorder(context, "oni");
            //context.CreateProductionTree(record.Info);
            //this.record.SetDestination(RecordMedium.File, "test.oni");

            this.generators = new ConcurrentBag<IGenerator>();
        }

        public void Add(IGenerator generator)
        {
            if (generator is DepthGeneratorAdapter)
            {
                DepthGeneratorAdapter depthGen = (DepthGeneratorAdapter)generator;
                //this.record.AddNodeToRecording(depthGen.Generator);
            }
            
            this.generators.Add(generator);
        }

        public bool IsRunning
        {
            get { return this.thread != null; }
        }

        public void Start()
        {
            
            this.thread = new Thread(new ThreadStart(Run));
            this.run = true;
            this.thread.Start();
        }

        public void Stop()
        {
            if (this.thread != null)
            {
                this.run = false;
                this.thread.Join();
                this.thread = null;
                //this.record.Dispose();
            }
        }

        [HandleProcessCorruptedStateExceptions]
        private void Run()
        {
            while (this.run)
            {
                try
                {
                    if (player == null)
                        this.context.WaitAnyUpdateAll();
                    else
                    {                                                
                        this.player.ReadNext();                        
                    }
                    foreach (var generator in this.generators)
                    {
                        if (player != null)
                        {
                            if (generator is DepthGeneratorAdapter)
                            {
                                this.context.WaitOneUpdateAll(((DepthGeneratorAdapter)generator).Generator);
                            }
                            else if (generator is ImageGeneratorAdapter)
                            {
                                this.context.WaitOneUpdateAll(((ImageGeneratorAdapter)generator).Generator);
                            }
                        }
                        
                        generator.Update();                        
                        //this.record.Record();
                    }
                }
                catch (StatusException)
                { }
                catch (AccessViolationException)
                { }
                catch (SEHException)
                { }
            }
        }
    }
}

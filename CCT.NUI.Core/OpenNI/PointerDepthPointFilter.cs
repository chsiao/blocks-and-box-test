﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CCT.NUI.Core.Clustering;
using System.Runtime.ExceptionServices;
using System.Runtime.InteropServices;
using Emgu;
using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.Util;
using Emgu.CV.Structure;
using Emgu.CV.VideoSurveillance;
using System.Drawing;
using System.Drawing.Imaging;



namespace CCT.NUI.Core.OpenNI
{
    public class PointerDepthPointFilter : IDepthPointFilter<IntPtr>
    {
        private int minimumDepthThreshold;
        private int maximumDepthThreshold;
        private int lowerBorder;
        private IntSize size;
        private int[,] presetModel = new int[640, 480];
        private Int16[] presetModel1D = new Int16[640 * 480];
        private Int16[] presetModelBox = new Int16[640 * 480];
        private Int16[] partitionModel = new Int16[640 * 480];
        private int count = 0;        

        public PointerDepthPointFilter(IntSize size, int minimumDepthThreshold, int maximumDepthThreshold, int lowerBorder)
        {
            this.size = size;
            this.minimumDepthThreshold = minimumDepthThreshold;
            this.maximumDepthThreshold = maximumDepthThreshold;
            this.lowerBorder = lowerBorder;
            
        }

 

        [HandleProcessCorruptedStateExceptions]
        public unsafe IList<Point> Filter(IntPtr source)
        {
            if (GlobalVariables.ResetDepthFilter)
            {
                count = 0;
                presetModel = new int[640, 480];
                presetModel1D = new Int16[640 * 480];
                presetModelBox = new Int16[640 * 480];
                partitionModel = new Int16[640 * 480];
                GlobalVariables.ResetDepthFilter = false;
            }

            var result = new List<Point>();
            try
            {
                ushort* pDepth = (ushort*)source.ToPointer();

                int localHeight = this.size.Height; //5ms faster when it's a local variable
                int localWidth = this.size.Width;
                int maxY = localHeight - lowerBorder;
                int minDepth = minimumDepthThreshold;
                int maxDepth = maximumDepthThreshold;

                List<string[]> output = new List<string[]>();


                for (int y = 0; y < localHeight; y++)
                {
                    string[] c = new string[640];
                    for (int x = 0; x < localWidth; x++)
                    {
                        ushort depthValue = *pDepth;

                        //------------------------modified by Chen
                        if (count == 0)
                            presetModel[x, y] = *pDepth;
                        else if (count < GlobalVariables.Counter)
                        {
                            presetModel[x, y] = (*pDepth + presetModel[x, y] * count) / (count + 1);
                        }

                        if (x == 235 && y == 93)
                        {

                        }

                        if (!GlobalVariables.isCalibrating)
                        {
                            if (count == GlobalVariables.Counter)
                            {

                                int a = presetModel[x, y];
                                string b = new string(a.ToString().ToCharArray());
                                c[x] = b;
                                presetModel1D[x + y * 640] = (Int16)presetModel[x, y];



                                if (presetModel[x, y] < GlobalVariables.CameraHeight && x > GlobalVariables.FilteredXmin && x < GlobalVariables.FilteredXmax)
                                {
                                    presetModelBox[x + y * 640] = (Int16)presetModel[x, y];
                                }
                                else
                                    presetModelBox[x + y * 640] = 0;

                                if (presetModel[x, y] < GlobalVariables.PartitionHeight && x > GlobalVariables.FilteredXmin && x < GlobalVariables.FilteredXmax)
                                {
                                    partitionModel[x + y * 640] = (Int16)presetModel[x, y];
                                }
                                else
                                    partitionModel[x + y * 640] = 0;

                            }

                            if (count > GlobalVariables.Counter)
                            {
                                //if (depthValue >= minDepth && depthValue <= maxDepth)// && y < maxY ) //Should not be put in a seperate method for performance reasons

                                if (depthValue >= 200 && depthValue < presetModel[x, y] - 20 && x < GlobalVariables.FilteredXmax && x > GlobalVariables.FilteredXmin)
                                {
                                    result.Add(new Point(x, y, depthValue));
                                }
                            }
                            //------------------------end
                        }
                        pDepth++;

                    }
                    output.Add(c);
                }

                if (count == GlobalVariables.Counter)
                {
                    FindBox(presetModelBox);
                }



                count++; //By Chen


            }

            catch (AccessViolationException ex1)
            {

            }
            catch (SEHException ex2)
            {

            }

            return result;
        }

        private void FindBox(short[] model)
        {
            //CSVUtil.WriteCSV("d:\\test.csv", output);

            Image<Gray, Int16> EMGUImage = new Image<Gray, Int16>(640, 480);
            GCHandle handle = GCHandle.Alloc(model, GCHandleType.Pinned);
            int size = Marshal.SizeOf(typeof(MIplImage));
            IntPtr imageHeaderForBytes = Marshal.AllocHGlobal(size);
            CvInvoke.cvInitImageHeader(
                  imageHeaderForBytes,
                  new System.Drawing.Size(640, 480),
                  Emgu.CV.CvEnum.IPL_DEPTH.IPL_DEPTH_16S, 1, 0, 4);
            int offset = (int)Marshal.OffsetOf(typeof(MIplImage), "imageData");
            Marshal.WriteIntPtr(
                  imageHeaderForBytes,
                  offset,
                  handle.AddrOfPinnedObject());

            CvInvoke.cvCopy(imageHeaderForBytes, EMGUImage.Ptr, IntPtr.Zero);

            Marshal.FreeHGlobal(imageHeaderForBytes);
            handle.Free();

            EMGUImage = EMGUImage.Erode(1);
            EMGUImage = EMGUImage.Dilate(1);

            Image<Gray, byte> canny = EMGUImage.Convert<Gray, byte>().SmoothMedian(7).Canny(new Gray(800), new Gray(200));

            #region Find the border of box

            Contour<System.Drawing.Point> blocks;
            blocks =
            canny.FindContours(Emgu.CV.CvEnum.CHAIN_APPROX_METHOD.CV_CHAIN_APPROX_SIMPLE, Emgu.CV.CvEnum.RETR_TYPE.CV_RETR_EXTERNAL);
            //-----------------------
            MCvFont font = new MCvFont(FONT.CV_FONT_HERSHEY_SIMPLEX, 0.5, 0.5);
            
            if (blocks == null)
                count = 0;

            for (; blocks != null; blocks = blocks.HNext)
            {
                Contour<System.Drawing.Point> poly = blocks
                    .ApproxPoly(blocks.Perimeter * 0.05,
                                blocks.Storage);
                if (poly.Area > 50000 && poly.Total > 3)// && poly.Perimeter <200)// && poly.Convex == true)
                {
                    EMGUImage.Draw(poly, new Gray(255), 5);
                    System.Drawing.Point[] fillArea = poly.ToArray();
                    int boardThickness = (int)(poly.BoundingRectangle.Width * 0.07F);
                    EMGUImage.FillConvexPoly(fillArea, new Gray(850));

                    fillArea[0].Y = poly.BoundingRectangle.Top + poly.BoundingRectangle.Height / 2 - boardThickness;
                    fillArea[1].Y = poly.BoundingRectangle.Top + poly.BoundingRectangle.Height / 2 + boardThickness;
                    fillArea[2].Y = poly.BoundingRectangle.Top + poly.BoundingRectangle.Height / 2 + boardThickness;
                    fillArea[3].Y = poly.BoundingRectangle.Top + poly.BoundingRectangle.Height / 2 - boardThickness;

                    fillArea[0].X = poly.BoundingRectangle.Right + 20;
                    fillArea[1].X = poly.BoundingRectangle.Right + 20;
                    fillArea[2].X = poly.BoundingRectangle.Left - 20;
                    fillArea[3].X = poly.BoundingRectangle.Left - 20;

                    EMGUImage.FillConvexPoly(fillArea, new Gray(750));

                    EMGUImage._Dilate(3);

                    Rectangle rightBox = new Rectangle();
                    rightBox.Location = new Point(poly.BoundingRectangle.Left + boardThickness, poly.BoundingRectangle.Top + boardThickness, 0);
                    rightBox.Size = new Size(poly.BoundingRectangle.Height / 2 - boardThickness, poly.BoundingRectangle.Width - boardThickness * 2 + 5);

                    Rectangle leftBox = new Rectangle();
                    leftBox.Location = new Point(poly.BoundingRectangle.Left + boardThickness, poly.BoundingRectangle.Top + poly.BoundingRectangle.Height / 2, 0);
                    leftBox.Size = new Size(poly.BoundingRectangle.Height / 2 - boardThickness, poly.BoundingRectangle.Width - boardThickness * 2);

                    GlobalVariables.TheBox.midBulkhead.X = (poly.BoundingRectangle.Right + poly.BoundingRectangle.Left) / 2;
                    GlobalVariables.TheBox.midBulkhead.Y = poly.BoundingRectangle.Top + poly.BoundingRectangle.Height / 2;
                    GlobalVariables.TheBox.rightBox = rightBox;
                    GlobalVariables.TheBox.leftBox = leftBox;


                    GlobalVariables.TheBox.midBulkhead.X = (poly.BoundingRectangle.Right + poly.BoundingRectangle.Left) / 2;
                    GlobalVariables.TheBox.midBulkhead.Y = poly.BoundingRectangle.Top + poly.BoundingRectangle.Height / 2;
                    GlobalVariables.TheBox.rightBox = rightBox;
                    GlobalVariables.TheBox.leftBox = leftBox;
                }
            }


            #endregion


            for (int y = 0; y < 480; y++)
            {
                for (int x = 0; x < 640; x++)
                {
                    if (EMGUImage.Data[y, x, 0] > 500)
                    {
                        presetModel[x, y] = EMGUImage.Data[y, x, 0];
                    }

                }
            }
            CvInvoke.cvShowImage("mask Snap Shot1", EMGUImage.Convert<Gray, byte>());
            //CvInvoke.cvShowImage("canny Snap Shot", canny);
            CvInvoke.cvWaitKey(100);

        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using CCT.NUI.Visual;
using CCT.NUI.Core;
using CCT.NUI.Core.Video;
using Emgu.CV;
using Emgu.Util;
using Emgu.CV.Structure;
using Emgu.CV.CvEnum;
using System.Runtime.InteropServices;

namespace CCT.NUI.Visual
{


    public partial class VideoControl : UserControl
    {
        private IBitmapDataSource imageSource;
        private volatile Bitmap bitmap = null;
        public bool ShowDebug = false;
        private IList<ILayer> layers = new List<ILayer>();
        public CCT.NUI.Core.Rectangle curBox = new Core.Rectangle();
        private bool boxisSet = false;

        public struct Blocks
        {
            public int lvl1blocks;
            public int lvl2blocks;
            public int lvl3blocks;
        }

        public Blocks blocks = new Blocks();

        public VideoControl()
        {
            InitializeComponent();
            this.Paint += new PaintEventHandler(VideoControl_Paint);
            this.MouseMove += new MouseEventHandler(VideoControl_MouseMove);
        }


        public VideoControl(IBitmapDataSource imageSource)
            : this()
        {
            this.SetImageSource(imageSource);
        }

        public System.Drawing.Point MouseLocation { get; private set; }

        public bool Stretch { get; set; }

        public void SetImageSource(IBitmapDataSource imageSource)
        {
            if (this.imageSource != null)
            {
                //imageSource.NewDataAvailable -= new NewDataHandler<Bitmap>(imageSource_NewImageAvailable);
                imageSource.NewBitmapsAvailable -= new NewMultiBitmapHandler<Bitmap>(imageSource_NewBitmapsAvailable);

            }
            this.imageSource = imageSource;
            //imageSource.NewDataAvailable += new NewDataHandler<Bitmap>(imageSource_NewImageAvailable);
            imageSource.NewBitmapsAvailable += new NewMultiBitmapHandler<Bitmap>(imageSource_NewBitmapsAvailable);
        }

        public void Clear()
        {
            this.bitmap = null;
            this.ClearLayers();
            this.Invalidate();
        }

        public void AddLayer(ILayer layer)
        {
            layer.RequestRefresh += new EventHandler(layer_RequestRefresh);
            this.layers.Add(layer);
        }

        public void ClearLayers()
        {
            foreach (var layer in this.layers)
            {
                layer.Dispose();
                layer.RequestRefresh -= new EventHandler(layer_RequestRefresh);
            }
            this.layers.Clear();
        }

        private Contour<System.Drawing.Point> blocksContour;
        public int count = 0;
        public void SetImage(Bitmap newImage, Bitmap lvl2 = null, Bitmap lvl3 = null)
        {
            lock (newImage)
            {
                this.bitmap = newImage;

                if (count > GlobalVariables.Counter)
                {
                    if (!boxisSet)
                    {
                        boxisSet = true;
                        this.curBox = GlobalVariables.TheBox.rightBox;
                    }

                    // level1
                    blocks.lvl1blocks = FindBlocks(newImage); //get the number of level1

                    // level2
                    if (lvl2 != null)
                        lock (lvl2)
                            blocks.lvl2blocks = FindBlocks(lvl2, 2);

                    // level3
                    if (lvl3 != null)
                        lock (lvl3)
                            blocks.lvl3blocks = FindBlocks(lvl3, 3);
                }
                else
                    count++;
            }
            this.Invalidate();
        }

        private int FindBlocks(Bitmap newImage, int level = 1)
        {
            Emgu.CV.Image<Bgra, byte> im = new Image<Bgra, byte>(newImage);
            im = im.Erode(3);
            im = im.Dilate(3);
            Emgu.CV.Image<Gray, byte> canny = im.Convert<Gray, byte>().Canny(new Gray(100), new Gray(100));
            //CvInvoke.cvShowImage("level " + level.ToString(), im);
            //CvInvoke.cvWaitKey(20);
            //--------------

            //--------------

            blocksContour =
            canny.FindContours(Emgu.CV.CvEnum.CHAIN_APPROX_METHOD.CV_CHAIN_APPROX_SIMPLE, Emgu.CV.CvEnum.RETR_TYPE.CV_RETR_EXTERNAL);
            int counter = 0;
            //-----------------------
            MCvFont font = new MCvFont(FONT.CV_FONT_HERSHEY_SIMPLEX, 0.5, 0.5);
            for (; blocksContour != null; blocksContour = blocksContour.HNext)
            {
                //Contour<System.Drawing.Point> poly = blocksContour
                //    .ApproxPoly(blocksContour.Perimeter * 0.05,
                //                blocksContour.Storage);

                //System.Diagnostics.Debug.WriteLine(poly.Area);

                #region algorithm 1
                //System.Drawing.Point ctrPoint = new System.Drawing.Point();
                //ctrPoint.X = blocksContour.BoundingRectangle.Location.X + blocksContour.BoundingRectangle.Width / 2;
                //ctrPoint.Y = blocksContour.BoundingRectangle.Location.Y + blocksContour.BoundingRectangle.Height / 2;

                //if (blocksContour.Area > 150 && blocksContour.Total > 3
                //    && ctrPoint.Y > curBox.Location.Y
                //    && ctrPoint.Y < curBox.Location.Y + curBox.Size.Height
                //    && ctrPoint.X > curBox.Location.X
                //    && ctrPoint.X < curBox.Location.X + curBox.Size.Width)// && poly.Perimeter <200)// && poly.Convex == true)
                //{
                //    im.Draw(blocksContour, new Bgra(0, 255, 255, 255), 1);

                //    System.Drawing.Point bottomLeft = new System.Drawing.Point(blocksContour.BoundingRectangle.Left, blocksContour.BoundingRectangle.Bottom);
                //    im.Draw(blocksContour.Area.ToString(), ref font, bottomLeft, new Bgra(255, 0, 0, 255));

                //    counter += (int)Math.Ceiling((Double)blocksContour.Area / (Double)340);
                //}
                #endregion

                #region algorithm 2
                if (blocksContour.Area > 150 && blocksContour.Total > 3 && blocksContour.BoundingRectangle.Top > curBox.Location.Y
                    && blocksContour.BoundingRectangle.Bottom < curBox.Location.Y + curBox.Size.Height
                    && blocksContour.BoundingRectangle.Left > curBox.Location.X
                    && blocksContour.BoundingRectangle.Right < curBox.Location.X + curBox.Size.Width)// && poly.Perimeter <200)// && poly.Convex == true)
                {
                    im.Draw(blocksContour, new Bgra(0, 255, 255, 255), 1);

                    System.Drawing.Point bottomLeft = new System.Drawing.Point(blocksContour.BoundingRectangle.Left, blocksContour.BoundingRectangle.Bottom);
                    im.Draw(blocksContour.Area.ToString(), ref font, bottomLeft, new Bgra(255, 0, 0, 255));

                    counter += (int)Math.Ceiling((Double)blocksContour.Area / (Double)345.0);
                }
                #endregion
            }

            if (ShowDebug)
            {
                font = new MCvFont(FONT.CV_FONT_HERSHEY_SIMPLEX, 5, 5);
                DrawBoxForDebug(im);
                im.Draw(counter.ToString(), ref font, new System.Drawing.Point(20, 240), new Bgra(255, 255, 255, 255));
                CvInvoke.cvShowImage("im on level " + level, im);
                CvInvoke.cvWaitKey(20);
            }

            return counter;
        }

        private void DrawBoxForDebug(Emgu.CV.Image<Bgra, byte> im)
        {
            System.Drawing.Rectangle rRect = new System.Drawing.Rectangle((int)GlobalVariables.TheBox.rightBox.Location.X, (int)GlobalVariables.TheBox.rightBox.Location.Y,
                (int)GlobalVariables.TheBox.rightBox.Size.Width, (int)GlobalVariables.TheBox.rightBox.Size.Height);
            System.Drawing.Rectangle lRect = new System.Drawing.Rectangle((int)GlobalVariables.TheBox.leftBox.Location.X, (int)GlobalVariables.TheBox.leftBox.Location.Y,
                (int)GlobalVariables.TheBox.leftBox.Size.Width, (int)GlobalVariables.TheBox.leftBox.Size.Height);

            im.Draw(rRect, new Bgra(0, 0, 255, 255), 1);
            im.Draw(lRect, new Bgra(0, 0, 255, 255), 1);
        }


        public event EventHandler NewMouseLocation;

        private void VideoControl_MouseMove(object sender, MouseEventArgs e)
        {
            this.MouseLocation = e.Location;
            if (this.NewMouseLocation != null)
            {
                this.NewMouseLocation(this, EventArgs.Empty);
            }
        }

        void VideoControl_Paint(object sender, PaintEventArgs e)
        {
            if (this.bitmap != null)
            {
                System.Drawing.Rectangle targetArea = this.GetTargetArea();
                lock (this.bitmap)
                {
                    e.Graphics.DrawImage(this.bitmap, targetArea);
                    if (GlobalVariables.isCalibrating)
                    {
                        System.Drawing.Rectangle rect = new System.Drawing.Rectangle(187, 41, 203, 384);
                        e.Graphics.DrawRectangle(Pens.Red, rect);
                    }
                    if (blocksContour != null)
                    {
                        for (; blocksContour != null; blocksContour = blocksContour.HNext)
                        {
                            Contour<System.Drawing.Point> poly = blocksContour
                                .ApproxPoly(blocksContour.Perimeter * 0.05,
                                            blocksContour.Storage);
                            if (poly.Area > 150 && poly.BoundingRectangle.Top > 65 && poly.BoundingRectangle.Bottom < 230
                            && poly.BoundingRectangle.Left > 180 && poly.BoundingRectangle.Right < 350)
                            {
                                e.Graphics.DrawRectangle(Pens.Yellow, blocksContour.BoundingRectangle);
                            }
                        }

                    }

                    //lbCount.Text = counter.ToString();
                }
                foreach (var layer in this.layers)
                {
                    layer.Paint(e.Graphics);
                }
            }
        }

        private System.Drawing.Rectangle GetTargetArea()
        {
            if (this.Stretch)
            {
                return this.ClientRectangle;
            }
            else
            {
                System.Drawing.Rectangle targetArea;
                if (this.imageSource == null)
                {
                    targetArea = new System.Drawing.Rectangle(0, 0, 640, 480);
                }
                else
                {
                    targetArea = new System.Drawing.Rectangle(0, 0, this.imageSource.Width, this.imageSource.Height);
                }
                if (targetArea.Width > this.Width || targetArea.Height > this.Height)
                {
                    decimal ratio = Math.Min((decimal)this.Width / targetArea.Width, (decimal)this.Height / targetArea.Height);
                    targetArea = new System.Drawing.Rectangle(0, 0, (int)(ratio * targetArea.Width), (int)(ratio * targetArea.Height));
                }
                return targetArea;
            }
        }

        void layer_RequestRefresh(object sender, EventArgs e)
        {
            this.Invalidate();
        }

        void imageSource_NewImageAvailable(Bitmap newImage)
        {
            this.SetImage(newImage);
        }

        void imageSource_NewBitmapsAvailable(List<Bitmap> listBitMaps)
        {
            if (listBitMaps.Count == 1)
                this.SetImage(listBitMaps.First());
            else
                this.SetImage(listBitMaps[0], listBitMaps[1], listBitMaps[2]);
        }


        private void VideoControl_Load(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void VideoControl_MouseMove_1(object sender, MouseEventArgs e)
        {
            lbXYmouse.Text = e.X.ToString() + "," + e.Y.ToString();
        }



    }
}

﻿namespace CCT.NUI.Visual
{
    partial class VideoControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbCount = new System.Windows.Forms.Label();
            this.lbXYmouse = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lbCount
            // 
            this.lbCount.AutoSize = true;
            this.lbCount.ForeColor = System.Drawing.Color.White;
            this.lbCount.Location = new System.Drawing.Point(13, 14);
            this.lbCount.Name = "lbCount";
            this.lbCount.Size = new System.Drawing.Size(13, 13);
            this.lbCount.TabIndex = 0;
            this.lbCount.Text = "0";
            // 
            // lbXYmouse
            // 
            this.lbXYmouse.AutoSize = true;
            this.lbXYmouse.ForeColor = System.Drawing.Color.White;
            this.lbXYmouse.Location = new System.Drawing.Point(528, 13);
            this.lbXYmouse.Name = "lbXYmouse";
            this.lbXYmouse.Size = new System.Drawing.Size(35, 13);
            this.lbXYmouse.TabIndex = 1;
            this.lbXYmouse.Text = "label1";
            this.lbXYmouse.Click += new System.EventHandler(this.label1_Click);
            // 
            // VideoControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lbXYmouse);
            this.Controls.Add(this.lbCount);
            this.DoubleBuffered = true;
            this.Name = "VideoControl";
            this.Size = new System.Drawing.Size(602, 333);
            this.Load += new System.EventHandler(this.VideoControl_Load);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.VideoControl_MouseMove_1);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbCount;
        private System.Windows.Forms.Label lbXYmouse;
    }
}

﻿namespace CCT.NUI.Samples
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series3 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend2 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series4 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series5 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.buttonExit = new System.Windows.Forms.Button();
            this.buttonHandAndFinger = new System.Windows.Forms.Button();
            this.radioButtonOpenNI = new System.Windows.Forms.RadioButton();
            this.buttonImageManipulation = new System.Windows.Forms.Button();
            this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.chart2 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.lbLvl1 = new System.Windows.Forms.Label();
            this.lbLvl2 = new System.Windows.Forms.Label();
            this.lbLvl3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.lbBlocks = new System.Windows.Forms.Label();
            this.lbStatus = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.lbHandMoving = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.chkShowDebug = new System.Windows.Forms.CheckBox();
            this.lbHandLoc = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.rRightBt = new System.Windows.Forms.RadioButton();
            this.chkRecord = new System.Windows.Forms.CheckBox();
            this.chk_Calibration = new System.Windows.Forms.CheckBox();
            this.videoControl = new CCT.NUI.Visual.VideoControl();
            this.txtFileName = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonExit
            // 
            this.buttonExit.Location = new System.Drawing.Point(12, 455);
            this.buttonExit.Name = "buttonExit";
            this.buttonExit.Size = new System.Drawing.Size(203, 43);
            this.buttonExit.TabIndex = 11;
            this.buttonExit.Text = "Exit";
            this.buttonExit.UseVisualStyleBackColor = true;
            this.buttonExit.Click += new System.EventHandler(this.buttonExit_Click);
            // 
            // buttonHandAndFinger
            // 
            this.buttonHandAndFinger.Enabled = false;
            this.buttonHandAndFinger.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonHandAndFinger.Location = new System.Drawing.Point(12, 90);
            this.buttonHandAndFinger.Name = "buttonHandAndFinger";
            this.buttonHandAndFinger.Size = new System.Drawing.Size(203, 43);
            this.buttonHandAndFinger.TabIndex = 7;
            this.buttonHandAndFinger.Text = "Block and Box Test";
            this.buttonHandAndFinger.UseVisualStyleBackColor = true;
            this.buttonHandAndFinger.Click += new System.EventHandler(this.buttonHandAndFinger_Click);
            // 
            // radioButtonOpenNI
            // 
            this.radioButtonOpenNI.AutoSize = true;
            this.radioButtonOpenNI.Location = new System.Drawing.Point(15, 18);
            this.radioButtonOpenNI.Name = "radioButtonOpenNI";
            this.radioButtonOpenNI.Size = new System.Drawing.Size(62, 17);
            this.radioButtonOpenNI.TabIndex = 2;
            this.radioButtonOpenNI.Text = "OpenNI";
            this.radioButtonOpenNI.UseVisualStyleBackColor = true;
            this.radioButtonOpenNI.CheckedChanged += new System.EventHandler(this.radioButtonOpenNI_CheckedChanged);
            // 
            // buttonImageManipulation
            // 
            this.buttonImageManipulation.Enabled = false;
            this.buttonImageManipulation.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonImageManipulation.Location = new System.Drawing.Point(12, 188);
            this.buttonImageManipulation.Name = "buttonImageManipulation";
            this.buttonImageManipulation.Size = new System.Drawing.Size(203, 43);
            this.buttonImageManipulation.TabIndex = 8;
            this.buttonImageManipulation.Text = "Reset";
            this.buttonImageManipulation.UseVisualStyleBackColor = true;
            this.buttonImageManipulation.Click += new System.EventHandler(this.buttonImageManipulation_Click);
            // 
            // chart1
            // 
            this.chart1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            chartArea1.Name = "ChartArea1";
            this.chart1.ChartAreas.Add(chartArea1);
            legend1.Name = "Legend1";
            this.chart1.Legends.Add(legend1);
            this.chart1.Location = new System.Drawing.Point(3, 3);
            this.chart1.Name = "chart1";
            series1.ChartArea = "ChartArea1";
            series1.Legend = "Legend1";
            series1.MarkerSize = 10;
            series1.Name = "Raw Data";
            series2.ChartArea = "ChartArea1";
            series2.Legend = "Legend1";
            series2.MarkerSize = 10;
            series2.Name = "Position of Hand";
            series3.ChartArea = "ChartArea1";
            series3.Enabled = false;
            series3.Legend = "Legend1";
            series3.MarkerSize = 10;
            series3.Name = "RectCenter";
            this.chart1.Series.Add(series1);
            this.chart1.Series.Add(series2);
            this.chart1.Series.Add(series3);
            this.chart1.Size = new System.Drawing.Size(529, 231);
            this.chart1.TabIndex = 15;
            this.chart1.Text = "chart1";
            this.chart1.Click += new System.EventHandler(this.chart1_Click);
            // 
            // chart2
            // 
            this.chart2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            chartArea2.Name = "ChartArea1";
            this.chart2.ChartAreas.Add(chartArea2);
            legend2.Name = "Legend1";
            this.chart2.Legends.Add(legend2);
            this.chart2.Location = new System.Drawing.Point(3, 3);
            this.chart2.Name = "chart2";
            series4.ChartArea = "ChartArea1";
            series4.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series4.Legend = "Legend1";
            series4.Name = "Height";
            series5.ChartArea = "ChartArea1";
            series5.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series5.Legend = "Legend1";
            series5.Name = "Raw Data";
            this.chart2.Series.Add(series4);
            this.chart2.Series.Add(series5);
            this.chart2.Size = new System.Drawing.Size(542, 237);
            this.chart2.TabIndex = 16;
            this.chart2.Text = "chart2";
            this.chart2.Click += new System.EventHandler(this.chart2_Click);
            // 
            // lbLvl1
            // 
            this.lbLvl1.AutoSize = true;
            this.lbLvl1.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.lbLvl1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbLvl1.ForeColor = System.Drawing.Color.White;
            this.lbLvl1.Location = new System.Drawing.Point(275, 444);
            this.lbLvl1.Name = "lbLvl1";
            this.lbLvl1.Size = new System.Drawing.Size(35, 13);
            this.lbLvl1.TabIndex = 17;
            this.lbLvl1.Text = "label2";
            // 
            // lbLvl2
            // 
            this.lbLvl2.AutoSize = true;
            this.lbLvl2.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.lbLvl2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbLvl2.ForeColor = System.Drawing.Color.White;
            this.lbLvl2.Location = new System.Drawing.Point(275, 459);
            this.lbLvl2.Name = "lbLvl2";
            this.lbLvl2.Size = new System.Drawing.Size(35, 13);
            this.lbLvl2.TabIndex = 18;
            this.lbLvl2.Text = "label2";
            // 
            // lbLvl3
            // 
            this.lbLvl3.AutoSize = true;
            this.lbLvl3.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.lbLvl3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbLvl3.ForeColor = System.Drawing.Color.White;
            this.lbLvl3.Location = new System.Drawing.Point(275, 474);
            this.lbLvl3.Name = "lbLvl3";
            this.lbLvl3.Size = new System.Drawing.Size(35, 13);
            this.lbLvl3.TabIndex = 19;
            this.lbLvl3.Text = "label2";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(229, 444);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(45, 13);
            this.label2.TabIndex = 20;
            this.label2.Text = "Level 1:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(229, 459);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(45, 13);
            this.label3.TabIndex = 21;
            this.label3.Text = "Level 2:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(229, 474);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(45, 13);
            this.label4.TabIndex = 22;
            this.label4.Text = "Level 3:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(867, 18);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(83, 24);
            this.label5.TabIndex = 23;
            this.label5.Text = "Blocks: ";
            this.label5.Visible = false;
            // 
            // lbBlocks
            // 
            this.lbBlocks.AutoSize = true;
            this.lbBlocks.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbBlocks.Location = new System.Drawing.Point(1035, 18);
            this.lbBlocks.Name = "lbBlocks";
            this.lbBlocks.Size = new System.Drawing.Size(21, 24);
            this.lbBlocks.TabIndex = 24;
            this.lbBlocks.Text = "0";
            this.lbBlocks.Visible = false;
            // 
            // lbStatus
            // 
            this.lbStatus.AutoSize = true;
            this.lbStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbStatus.Location = new System.Drawing.Point(1035, 62);
            this.lbStatus.Name = "lbStatus";
            this.lbStatus.Size = new System.Drawing.Size(49, 24);
            this.lbStatus.TabIndex = 26;
            this.lbStatus.Text = "stop";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(867, 62);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(145, 24);
            this.label7.TabIndex = 25;
            this.label7.Text = "System Status:";
            // 
            // lbHandMoving
            // 
            this.lbHandMoving.AutoSize = true;
            this.lbHandMoving.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbHandMoving.Location = new System.Drawing.Point(1035, 18);
            this.lbHandMoving.Name = "lbHandMoving";
            this.lbHandMoving.Size = new System.Drawing.Size(21, 24);
            this.lbHandMoving.TabIndex = 28;
            this.lbHandMoving.Text = "0";
            this.lbHandMoving.Click += new System.EventHandler(this.label6_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(867, 18);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(77, 24);
            this.label8.TabIndex = 27;
            this.label8.Text = "Blocks:";
            this.label8.Click += new System.EventHandler(this.label8_Click);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer1.Location = new System.Drawing.Point(13, 504);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.chart1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.chart2);
            this.splitContainer1.Size = new System.Drawing.Size(1087, 243);
            this.splitContainer1.SplitterDistance = 535;
            this.splitContainer1.TabIndex = 29;
            // 
            // chkShowDebug
            // 
            this.chkShowDebug.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkShowDebug.Enabled = false;
            this.chkShowDebug.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.chkShowDebug.Location = new System.Drawing.Point(12, 139);
            this.chkShowDebug.Name = "chkShowDebug";
            this.chkShowDebug.Size = new System.Drawing.Size(203, 43);
            this.chkShowDebug.TabIndex = 30;
            this.chkShowDebug.Text = "Show Debug Images";
            this.chkShowDebug.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkShowDebug.UseVisualStyleBackColor = true;
            this.chkShowDebug.CheckedChanged += new System.EventHandler(this.chkShowDebug_CheckedChanged);
            // 
            // lbHandLoc
            // 
            this.lbHandLoc.AutoSize = true;
            this.lbHandLoc.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbHandLoc.Location = new System.Drawing.Point(995, 109);
            this.lbHandLoc.Name = "lbHandLoc";
            this.lbHandLoc.Size = new System.Drawing.Size(0, 24);
            this.lbHandLoc.TabIndex = 32;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(867, 109);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(220, 24);
            this.label9.TabIndex = 31;
            this.label9.Text = "Hand on the          box";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.radioButton1);
            this.panel1.Controls.Add(this.rRightBt);
            this.panel1.Enabled = false;
            this.panel1.Location = new System.Drawing.Point(12, 43);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(203, 43);
            this.panel1.TabIndex = 33;
            // 
            // radioButton1
            // 
            this.radioButton1.Appearance = System.Windows.Forms.Appearance.Button;
            this.radioButton1.Location = new System.Drawing.Point(4, 3);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(96, 37);
            this.radioButton1.TabIndex = 1;
            this.radioButton1.Text = "Left";
            this.radioButton1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.radioButton1.UseVisualStyleBackColor = true;
            // 
            // rRightBt
            // 
            this.rRightBt.Appearance = System.Windows.Forms.Appearance.Button;
            this.rRightBt.Checked = true;
            this.rRightBt.Location = new System.Drawing.Point(106, 3);
            this.rRightBt.Name = "rRightBt";
            this.rRightBt.Size = new System.Drawing.Size(94, 37);
            this.rRightBt.TabIndex = 0;
            this.rRightBt.TabStop = true;
            this.rRightBt.Text = "Right";
            this.rRightBt.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.rRightBt.UseVisualStyleBackColor = true;
            this.rRightBt.CheckedChanged += new System.EventHandler(this.rRightBt_CheckedChanged);
            // 
            // chkRecord
            // 
            this.chkRecord.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkRecord.Location = new System.Drawing.Point(12, 377);
            this.chkRecord.Name = "chkRecord";
            this.chkRecord.Size = new System.Drawing.Size(104, 35);
            this.chkRecord.TabIndex = 34;
            this.chkRecord.Text = "Load File";
            this.chkRecord.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkRecord.UseVisualStyleBackColor = true;
            this.chkRecord.CheckedChanged += new System.EventHandler(this.chkRecord_CheckedChanged);
            // 
            // chk_Calibration
            // 
            this.chk_Calibration.AutoSize = true;
            this.chk_Calibration.Location = new System.Drawing.Point(16, 237);
            this.chk_Calibration.Name = "chk_Calibration";
            this.chk_Calibration.Size = new System.Drawing.Size(75, 17);
            this.chk_Calibration.TabIndex = 35;
            this.chk_Calibration.Text = "Calibration";
            this.chk_Calibration.UseVisualStyleBackColor = true;
            this.chk_Calibration.CheckedChanged += new System.EventHandler(this.chk_Calibration_CheckedChanged);
            // 
            // videoControl
            // 
            this.videoControl.BackColor = System.Drawing.Color.Black;
            this.videoControl.Location = new System.Drawing.Point(221, 18);
            this.videoControl.Name = "videoControl";
            this.videoControl.Size = new System.Drawing.Size(640, 480);
            this.videoControl.Stretch = false;
            this.videoControl.TabIndex = 8;
            // 
            // txtFileName
            // 
            this.txtFileName.Enabled = false;
            this.txtFileName.Location = new System.Drawing.Point(12, 419);
            this.txtFileName.Name = "txtFileName";
            this.txtFileName.Size = new System.Drawing.Size(203, 20);
            this.txtFileName.TabIndex = 35;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1103, 750);
            this.Controls.Add(this.txtFileName);
            this.Controls.Add(this.chkRecord);
            this.Controls.Add(this.chk_Calibration);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.lbHandLoc);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.chkShowDebug);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.lbHandMoving);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.lbStatus);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.lbBlocks);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lbLvl3);
            this.Controls.Add(this.lbLvl2);
            this.Controls.Add(this.lbLvl1);
            this.Controls.Add(this.buttonImageManipulation);
            this.Controls.Add(this.radioButtonOpenNI);
            this.Controls.Add(this.buttonHandAndFinger);
            this.Controls.Add(this.videoControl);
            this.Controls.Add(this.buttonExit);
            this.DoubleBuffered = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainForm";
            this.Text = "Blocks and Box Test";
            this.Load += new System.EventHandler(this.MainForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart2)).EndInit();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonExit;
        private CCT.NUI.Visual.VideoControl videoControl;
        private System.Windows.Forms.Button buttonHandAndFinger;
        private System.Windows.Forms.RadioButton radioButtonOpenNI;
        private System.Windows.Forms.Button buttonImageManipulation;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart1;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart2;
        private System.Windows.Forms.Label lbLvl1;
        private System.Windows.Forms.Label lbLvl2;
        private System.Windows.Forms.Label lbLvl3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lbBlocks;
        private System.Windows.Forms.Label lbStatus;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label lbHandMoving;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.CheckBox chkShowDebug;
        private System.Windows.Forms.Label lbHandLoc;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.RadioButton rRightBt;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.CheckBox chkRecord;
        private System.Windows.Forms.TextBox txtFileName;
        private System.Windows.Forms.CheckBox chk_Calibration;
    }
}


﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;
using System.IO;
using CCT.NUI.Visual;
using CCT.NUI.Core;
using CCT.NUI.Core.Clustering;
using CCT.NUI.HandTracking;
using CCT.NUI.Core.OpenNI;
using CCT.NUI.Core.Shape;
using CCT.NUI.Samples.ImageManipulation;
using CCT.NUI.KinectSDK;
using CCT.NUI.Core.Video;
using System.Threading;

namespace CCT.NUI.Samples
{
    public partial class MainForm : Form
    {
        
        private HandDataSource hands;
        private IList<IDataSource> activeDataSources;
        private IDataSourceFactory dataSourceFactory;

        private ClusterDataSourceSettings clusteringSettings = new ClusterDataSourceSettings();
        private ShapeDataSourceSettings shapeSettings = new ShapeDataSourceSettings();
        private HandDataSourceSettings handDetectionSettings = new HandDataSourceSettings();

        private delegate void ExtrudePolygonDelegate(Core.Point PalmLocation);

        public int lastNumBlocks = 0;
        public int numHandMove = 0;

        public enum HandPosition
        {
            left,
            right
        };

        public static HandPosition handPosition = HandPosition.left;
        public static int righthandVerification = 0;
        public static int lefthandVerification = 0;
        public static HandPosition lastHandPosition = HandPosition.left;

        public struct Blocks
        {
            public int lvl1;
            public int lvl2;
            public int lvl3;
        }

        #region Plot Variables
        double[] data_array_X = new double[100];
        double[] data_array_Y = new double[100];
        double[] data_array_XH = new double[100];
        double[] data_array_YH = new double[100];
        double[] Kalman_array_Y = new double[100];
        double[] Kalman_array_X = new double[100];
        double[] Kalman_array_Y1 = new double[100];
        double[] Kalman_array_X1 = new double[100];
        double[] Kalman_array_Y2 = new double[100];
        double[] Kalman_array_X2 = new double[100];
        KalmanFilterClass kalman = new KalmanFilterClass();
        KalmanFilterClass kalman1 = new KalmanFilterClass();
        KalmanFilterClass kalman2 = new KalmanFilterClass();
        KalmanFilterClass kalman3 = new KalmanFilterClass();
        PointF[] oup = new PointF[2];
        PointF[] oup1 = new PointF[2];
        PointF[] oup2 = new PointF[2];
        float ix = 100, iy,iy1,ix1,ix2,iy2,ixForCounting;

        int timeFromStart = 0;
        public string sysStatus = "";

        System.Timers.Timer HandWave_Timer = new System.Timers.Timer();
        #endregion

        public int yThreshold = new int();

        public MainForm()
        {
            InitializeComponent();
            this.activeDataSources = new List<IDataSource>();
            this.FormClosing += new FormClosingEventHandler(MainForm_FormClosing);
            
            SetupChart();
            kalman.KalmanFilter();
            kalman1.KalmanFilter();
            kalman2.KalmanFilter();
            kalman3.KalmanFilter();
        }

        private void buttonRGB_Click(object sender, EventArgs e)
        {
            this.SetImageDataSource(this.dataSourceFactory.CreateRGBBitmapDataSource());
        }

        private void buttonDepth_Click(object sender, EventArgs e)
        {
            this.SetImageDataSource(this.dataSourceFactory.CreateDepthBitmapDataSource());
        }

        private void buttonClustering_Click(object sender, EventArgs e)
        {
            this.SetClusterDataSource(this.dataSourceFactory.CreateClusterDataSource(this.clusteringSettings));
        }

        private void SetClusterDataSource(IClusterDataSource dataSource)
        {
            this.SetDataSource(dataSource, new ClusterLayer(dataSource));
        }

        private void SetHandDataSource(IHandDataSource dataSource)
        {
            this.SetDataSource(dataSource, new HandLayer(dataSource));
        }

        private void SetDataSource(IDataSource dataSource, ILayer layer)
        {
            this.Clear();
            this.activeDataSources.Add(dataSource);
            this.videoControl.AddLayer(layer);
            
            layer.RequestRefresh += new EventHandler(layer_RequestRefresh);
            dataSource.Start();
        }

        private void SetImageDataSource(IBitmapDataSource dataSource)
        {
            this.Clear();
            this.activeDataSources.Add(dataSource);
            this.videoControl.SetImageSource(dataSource);
            dataSource.Start();
        }

        private void Clear()
        {
            foreach (var dataSource in this.activeDataSources)
            {
                dataSource.Stop();
            }
            this.activeDataSources.Clear();
            this.videoControl.Clear();
        }

        void layer_RequestRefresh(object sender, EventArgs e)
        {
            //this.videoControl.Invalidate();

            if (hands.CurrentValue.Hands.Count > 0 && hands.CurrentValue.Hands.First().HasPalmPoint)
            {
                this.BeginInvoke(new ExtrudePolygonDelegate(updateData), hands.CurrentValue.Hands.First().PalmPoint);
                ix++;
                ix1++;
                ix2++;

                Thread newThread = new Thread(new ThreadStart(SetLabel));
                newThread.Start();
 
            }

        }
        #region setlabel

        void SetLabel()
        {
            CrossDelegate dl = new CrossDelegate(Done);
            Blocks blocks = new Blocks();
            blocks.lvl1 = this.videoControl.blocks.lvl1blocks;
            blocks.lvl2 = this.videoControl.blocks.lvl2blocks;
            blocks.lvl3 = this.videoControl.blocks.lvl3blocks;
            
            
            this.BeginInvoke(dl, blocks); 
        }

        private void Done(Blocks blocks)
        {
            lbLvl1.Text = blocks.lvl1.ToString();
            lbLvl2.Text = blocks.lvl2.ToString();
            lbLvl3.Text = blocks.lvl3.ToString();
            //PointF[] sum = kalman3.filterPoints(new PointF(ixForCounting++, (blocks.lvl1 + blocks.lvl2 + blocks.lvl3)));
            //lbBlocks.Text = (blocks.lvl1 + blocks.lvl2 + blocks.lvl3).ToString();
            
            int numBlocks = 0;
            //if (sum[1].Y - Math.Floor(sum[1].Y) > 0.5)
            //    numBlocks = (int)(Math.Floor(sum[1].Y) + 1);
                
            //else
            //    numBlocks = (int)(Math.Floor(sum[1].Y));

            if (handPosition == HandPosition.left)// && righthandVerification > 10)
            {
                lbBlocks.Text = (blocks.lvl1 + blocks.lvl2 + blocks.lvl3).ToString();//numBlocks.ToString();
                numBlocks = (blocks.lvl1 + blocks.lvl2 + blocks.lvl3);

                //System.Diagnostics.Debug.WriteLine("number of blocks: {0}; last number of blocks: {1}", numBlocks, lastNumBlocks);
                if (numBlocks > lastNumBlocks)
                {
                    numHandMove++;
                    if (numBlocks - lastNumBlocks > 5) lastNumBlocks++; // sometimes the depth image are not filtered correctly
                    else lastNumBlocks = numBlocks;

                    lbBlocks.Text = numBlocks.ToString();
                    lbHandMoving.Text = numHandMove.ToString();                    
                }
                
            }


            lbStatus.Text = "Running!";
        }


        public delegate void CrossDelegate(Blocks blocks);

        #endregion

        


        void updateData(Core.Point pt)
        {
            //lbXYhandcenter.Text = hands.CurrentValue.Hands.First().PalmX.ToString() + "," + hands.CurrentValue.Hands.First().PalmX.ToString();
            //HandWaveDisplay(new PointF((float)hands.CurrentValue.Hands.First().PalmX, (float)hands.CurrentValue.Hands.First().PalmY));
            HandWaveDisplay(new PointF((float)hands.CurrentValue.Hands.First().PalmX, (float)hands.CurrentValue.Hands.First().PalmY),
                new PointF((float)hands.CurrentValue.Hands.First().Location.X, (float)hands.CurrentValue.Hands.First().Location.Y)
                ,(int)hands.CurrentValue.Hands.First().Location.Z);
            
        }

        void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Clear();
            if (this.dataSourceFactory != null)
            {
                this.dataSourceFactory.DisposeAll();
            }
        }

        private void buttonExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void linkLabelSource_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start("http://candescentnui.codeplex.com/");
        }

        private void linkLabelBlog_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start("http://blog.candescent.ch");
        }

        private void buttonHandAndFinger_Click(object sender, EventArgs e)
        {   
            this.SetImageDataSource(this.dataSourceFactory.CreateDepthBitmapDataSource());
            //this.SetImageDataSource(this.dataSourceFactory.CreateRGBBitmapDataSource());
            hands = new HandDataSource(this.dataSourceFactory.CreateShapeDataSource(this.clusteringSettings, this.shapeSettings), this.handDetectionSettings);
  
            //this.SetHandDataSource(hands);

            this.activeDataSources.Add(hands);
            var layer = new HandLayer(hands);
            this.videoControl.AddLayer(layer);
            HandWave_Timer.Interval = 100;
            //HandWave_Timer.Tick += new EventHandler(layer_RequestRefresh);
            //HandWave_Timer.Start();
            layer.RequestRefresh += new EventHandler(layer_RequestRefresh);
            hands.Start();

            lbStatus.Text = "Initializing...";           
        }

        void MainForm_NewDataAvailable(System.Windows.Media.ImageSource data)
        {
            
        }

        private void buttonSettings_Click(object sender, EventArgs e)
        {
            new SettingsForm(this.clusteringSettings, this.shapeSettings, this.handDetectionSettings).Show();
        }

        private void radioButtonSDK_CheckedChanged(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            try
            {
                this.dataSourceFactory = new SDKDataSourceFactory();
            }
            catch (Exception exc)
            {
                Cursor.Current = Cursors.Default;
                MessageBox.Show(exc.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            this.ToggleButtons();
            Cursor.Current = Cursors.Default;
        }

        private void ToggleButtons()
        {
            this.Enable(this.panel1, this.chkShowDebug, this.buttonHandAndFinger, this.buttonImageManipulation);
            this.Disable(this.radioButtonOpenNI);
        }

        private void Enable(params Control[] controls)
        {
            this.SetEnabled(controls, true);
        }

        private void Disable(params Control[] controls)
        {
            this.SetEnabled(controls, false);
        }

        private void SetEnabled(IEnumerable<Control> controls, bool value)
        {
            foreach (var control in controls)
            {
                control.Enabled = value;
            }
        }

        private void radioButtonOpenNI_CheckedChanged(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            this.InitializeOpenNI();
            this.ToggleButtons();
            //this.buttonHandDataFactory.Enabled = true;
            Cursor.Current = Cursors.Default;
            
        }

        private void InitializeOpenNI()
        {
            try
            {
                this.dataSourceFactory = new OpenNIDataSourceFactory("Config.xml", this.txtFileName.Text);
            }
            catch (Exception exc)
            {
                Cursor.Current = Cursors.Default;
                MessageBox.Show(exc.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        private void buttonImageManipulation_Click(object sender, EventArgs e)
        {
            GlobalVariables.ResetDepthFilter = true;
            GlobalVariables.ResetDepthBitmap = true;
            videoControl.count = 0;
            this.lbHandMoving.Text = "0";

            //var dataSource = new HandDataSource(this.dataSourceFactory.CreateShapeDataSource(this.clusteringSettings, this.shapeSettings));
            //new ImageForm(dataSource).Show();
            //dataSource.Start();
        }

        private void buttonHandDataFactory_Click(object sender, EventArgs e)
        {
            var factory = new HandDataFactory(new IntSize(640, 480));
            
            var handData = factory.Create((this.dataSourceFactory as OpenNIDataSourceFactory).GetDepthGenerator().DataPtr);
            MessageBox.Show(string.Format("{0} hands detected", handData.Count), "Detection Message");
        }

        private void buttonHandTracking_Click(object sender, EventArgs e)
        {
            this.SetImageDataSource(this.dataSourceFactory.CreateDepthBitmapDataSource());
            var dataSource = (this.dataSourceFactory as OpenNIDataSourceFactory).CreateTrackingClusterDataSource();
            var handDataSource = new HandDataSource(this.dataSourceFactory.CreateShapeDataSource(dataSource, this.shapeSettings), this.handDetectionSettings);
            
            this.activeDataSources.Add(handDataSource);
            var layer = new HandLayer(handDataSource);
            this.videoControl.AddLayer(layer);
            handDataSource.Start();
        }

        private void radioOpenNINite_CheckedChanged(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            this.Disable(this.radioButtonOpenNI, this.radioButtonOpenNI);
            this.InitializeOpenNI();
            //this.buttonHandTracking.Enabled = true;
            //this.buttonHandDataFactory.Enabled = true;
            Cursor.Current = Cursors.Default;
        }

        public void updataControl(string txt)
        {
            //lbXYhandcenter.Text = txt;
        }

        public void SetupChart()
        {
            //Data 
            //Type and colour

            chart1.Series[0].Label = "The Data";
            chart1.Series[0].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.FastLine;
            chart1.Series[0].Color = Color.Yellow;

            
            chart2.Series[0].Label = "The Height";
            chart2.Series[0].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.FastLine;
            chart2.Series[0].Color = Color.Black;

            //Axis
            chart1.ChartAreas[0].AxisX.Title = "Time";
            chart1.ChartAreas[0].AxisY.Title = "Y axis";
           

            chart1.ChartAreas[0].AxisX.Minimum = 0;
            chart1.ChartAreas[0].AxisX.Maximum = data_array_X.Length - 1;

            chart2.ChartAreas[0].AxisX.Title = "Time";
            chart2.ChartAreas[0].AxisY.Title = "Height of hand";
            
            

            chart2.ChartAreas[0].AxisX.Minimum = 0;
            chart2.ChartAreas[0].AxisX.Maximum = data_array_X.Length - 1;

            //Kalman
            //Type and colour
            chart1.Series[1].Label = "Kalman Data";
            chart1.Series[1].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.FastLine;
            chart1.Series[1].Color = Color.Red;

            chart1.Series[2].Label = "RectCenter";
            chart1.Series[2].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.FastLine;
            chart1.Series[2].Color = Color.Blue;
            

            Update();
        }

        public void HandWaveDisplay(PointF rawpoint, PointF rawpoint2, int height)
        {
            //ix++;
            iy = (int)rawpoint.Y;
            //ix1++;
            iy1= (int)rawpoint2.Y;
            //ix2++;
            iy2 = height;

            //set data
            Array.Copy(data_array_Y, 1, data_array_Y, 0, 99);
            data_array_Y[data_array_Y.Length - 1] = (int)iy;
            Array.Copy(data_array_X, 1, data_array_X, 0, 99);
            data_array_X[data_array_X.Length - 1] = (int)ix;


            //set data
            Array.Copy(data_array_YH, 1, data_array_YH, 0, 99);
            data_array_YH[data_array_YH.Length - 1] = (int)iy2;
            Array.Copy(data_array_XH, 1, data_array_XH, 0, 99);
            data_array_XH[data_array_XH.Length - 1] = (int)ix2;

            

            //updatae kalman and predict
            PointF inp = new PointF(ix, iy);
            oup = new PointF[2];
            oup = kalman.filterPoints(inp);
            

            oup1 = new PointF[2];
            PointF inp1 = new PointF(ix1, iy1);
            oup1 = kalman1.filterPoints(inp1);

            oup2 = new PointF[2];
            PointF inp2 = new PointF(ix2, iy2);
            oup2 = kalman2.filterPoints(inp2);
            

            PointF[] pts = oup;

            //Set kalman Data
            Array.Copy(Kalman_array_Y, 1, Kalman_array_Y, 0, 99);
            Kalman_array_Y[Kalman_array_Y.Length - 1] = (int)oup[1].Y;
            Array.Copy(Kalman_array_X, 1, Kalman_array_X, 0, 99);
            Kalman_array_X[Kalman_array_X.Length - 1] = (int)oup[1].X;

            //Set kalman Data
            Array.Copy(Kalman_array_Y1, 1, Kalman_array_Y1, 0, 99);
            Kalman_array_Y1[Kalman_array_Y1.Length - 1] = (int)oup1[1].Y;
            Array.Copy(Kalman_array_X1, 1, Kalman_array_X1, 0, 99);
            Kalman_array_X1[Kalman_array_X1.Length - 1] = (int)oup1[1].X;

            //Set kalman Data
            Array.Copy(Kalman_array_Y2, 1, Kalman_array_Y2, 0, 99);
            Kalman_array_Y2[Kalman_array_Y2.Length - 1] = (int)oup2[1].Y;
            Array.Copy(Kalman_array_X2, 1, Kalman_array_X2, 0, 99);
            Kalman_array_X2[Kalman_array_X2.Length - 1] = (int)oup2[1].X;

            chart1.ChartAreas[0].AxisY.Maximum = Math.Max(Kalman_array_Y.Max(), Kalman_array_Y1.Max());
            chart1.ChartAreas[0].AxisY.Minimum = Math.Min(Kalman_array_Y.Min(), Kalman_array_Y1.Min());
            chart1.ChartAreas[0].AxisX.Maximum = Math.Max(Kalman_array_X.Max(), Kalman_array_X1.Max());
            chart1.ChartAreas[0].AxisX.Minimum = Math.Min(Kalman_array_X.Min(), Kalman_array_X1.Min());

            chart2.ChartAreas[0].AxisY.Maximum = Math.Max(Kalman_array_Y2.Max(), Kalman_array_Y2.Max());
            chart2.ChartAreas[0].AxisY.Minimum = Math.Min(Kalman_array_Y2.Min(), Kalman_array_Y2.Min());
            chart2.ChartAreas[0].AxisX.Maximum = (Kalman_array_X2.Max());
            chart2.ChartAreas[0].AxisX.Minimum = (Kalman_array_X2.Min());

            Update();
            this.Refresh();

            if (oup[1].Y < GlobalVariables.TheBox.midBulkhead.Y)
            {
                //if (righthandVerification > 10)
                {
                    handPosition = HandPosition.right;
                    lbHandLoc.Text = handPosition.ToString();
                    if(righthandVerification > 20)
                        lefthandVerification = 0;
                }
                righthandVerification++;
                //System.Diagnostics.Debug.WriteLine(handPosition);
            }
            else
            {
                if (lefthandVerification > 10)
                {
                    handPosition = HandPosition.left;
                    lbHandLoc.Text = handPosition.ToString();
                    if(lefthandVerification > 10)
                        righthandVerification = 0;
                }
                lefthandVerification++;
                //System.Diagnostics.Debug.WriteLine(handPosition);
            }
        }

        public void Update()
        {
            chart1.ChartAreas[0].AxisX.Minimum = data_array_X[0];

            chart1.Series[0].Points.DataBindXY(data_array_X, data_array_Y);
            chart2.Series[1].Points.DataBindXY(data_array_XH, data_array_YH);

            chart1.Series[1].Points.DataBindXY(Kalman_array_X, Kalman_array_Y);
            chart1.Series[2].Points.DataBindXY(Kalman_array_X1, Kalman_array_Y1); 
            chart2.Series[0].Points.DataBindXY(Kalman_array_X2, Kalman_array_Y2); // Axis-Z

        }

        private void chart1_Click(object sender, EventArgs e)
        {

        }

        private void chart2_Click(object sender, EventArgs e)
        {

        }

        private void MainForm_Load(object sender, EventArgs e)
        {

        }

        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void chkShowDebug_CheckedChanged(object sender, EventArgs e)
        {
            this.videoControl.ShowDebug = ((CheckBox)sender).Checked;
        }

        private void rRightBt_CheckedChanged(object sender, EventArgs e)
        {
            this.buttonImageManipulation_Click(sender, e);

            lastNumBlocks = 0;
            lbBlocks.Text = "0";
            lbHandMoving.Text = "0";
            this.videoControl.blocks.lvl1blocks = 0;
            this.videoControl.blocks.lvl2blocks = 0;
            this.videoControl.blocks.lvl3blocks = 0;

            if (!((RadioButton)sender).Checked)
                this.videoControl.curBox = GlobalVariables.TheBox.leftBox;
            else
                this.videoControl.curBox = GlobalVariables.TheBox.rightBox;
        }

        private void chkRecord_CheckedChanged(object sender, EventArgs e)
        {
            OpenFileDialog fDialog = new OpenFileDialog();
            fDialog.Title = "Open";
            fDialog.FileName = "";
            fDialog.DefaultExt = "oni";
            fDialog.Filter = "OpenNI File |*.oni";

            DialogResult dialogResult = fDialog.ShowDialog(this);

            switch (dialogResult)
            {
                case System.Windows.Forms.DialogResult.Cancel:
                    this.isPlayingOniFile = false;
                    break;
                default:
                    string fileName = fDialog.FileName;
                    this.txtFileName.Text = fileName;
                    radioOpenNINite_CheckedChanged(null, null);
                    buttonHandAndFinger_Click(null, null);
                    break;
            }
            
        }

        public bool isPlayingOniFile { get; set; }

        private void Calibration_Click(object sender, EventArgs e)
        {
            
        }

        private void chk_Calibration_CheckedChanged(object sender, EventArgs e)
        {
            if (!GlobalVariables.isCalibrating)
            {
                GlobalVariables.isCalibrating = true;
                this.SetImageDataSource(this.dataSourceFactory.CreateDepthBitmapDataSource());
            }
            else
            {
                GlobalVariables.isCalibrating = false;
            }
            
        }
    }
}
